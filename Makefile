#!make
include .env
SHELL := /bin/bash

detect_secrets_check: clean_detect_secrets_reports
	@echo "Scan du depôt à la recherche de credentials"	
	@mkdir -p ./reports/detect-secrets/
ifeq ($(DEBUG), 1)
	@echo "Execution dans un environnement de developpement"
	@docker build -t todo_scan_for_git_credentials -f ./build/detect-secrets/Dockerfile .
	@docker run -v ${PWD}:/app todo_scan_for_git_credentials detect-secrets scan . --all-files --exclude-files .*\.html$ > ./reports/detect-secrets/output.json
else 
	detect-secrets scan . --all-files --exclude-files .*\.html$ > ./reports/detect-secrets/output.json
endif

safety_check: clean_safety_reports
	@echo "Vérification des dépendances du projet"
	@mkdir -p ./reports/safety/
ifeq ($(DEBUG), 1)
	@echo "Execution dans un environnement de developpement"
	@docker build -t todo_dependancies_check -f ./build/safety/Dockerfile .
	@pipenv lock --requirements > requirements.txt
	@docker run -v ${PWD}:/app todo_dependancies_check safety check --json --full-report --output ./reports/safety/output.json -r requirements.txt || exit 0
	@rm requirements.txt
else 
	@pipenv lock --requirements > requirements.txt
	@safety check --json --full-report --output ./reports/safety/output.json -r requirements.txt || exit 0
	@rm requirements.txt
endif

flake8_check: clean_flake8_reports
	@echo "Lancement des tests statiques"
	@mkdir -p ./reports/flake8/
ifeq ($(DEBUG), 1)
	@echo "Execution dans un environnement de developpement"
	@docker build -t todo_code_quality_check -f ./build/flake8/Dockerfile .
	@docker run -v ${PWD}:/app todo_code_quality_check flake8 --max-line-length 120 --output-file /reports/flake8/output.xml ./src || exit 0
else 
	@flake8 --max-line-length 120 --output-file reports/flake8/output.xml ./src || exit 0
endif

unittest_check:
	@echo "Lancement des tests unitaire et de la couverture de code"
	@mkdir -p ./reports/unittest/
ifeq ($(DEBUG), 1)
	@pipenv install
	@echo "Execution dans un environnement de developpement"
	@pipenv run python src/manage.py makemigrations
	@pipenv run python src/manage.py migrate
	@docker build -t todo_code_coverage_check -f ./build/unittest/Dockerfile.dev .
	@docker run -v ${PWD}:/app todo_code_coverage_check coverage run --source='.' manage.py test || exit 0
	@docker run -v ${PWD}:/app todo_code_coverage_check coverage xml -o ../reports/unittest/coverage.xml 
	@docker run -v ${PWD}:/app todo_code_coverage_check coverage html -d ../reports/unittest/html
	@rm -f src/.coverage
	@rm -f src/TEST-tasks*
else
	@pipenv install --system --deploy --ignore-pipfile	
	@python src/manage.py makemigrations
	@python src/manage.py migrate
	@cd ./src/; coverage run --source='.' manage.py test 
	@cd ./src/; coverage html -d ../reports/unittest/html
	@cd ./src/; coverage xml -o ../reports/unittest/output.xml
endif


hadolint_check: clean_hadolint_reports
	@echo "Lancement des tests statiques sur les Dockerfiles"
	@mkdir -p ./reports/hadolint/
ifeq ($(DEBUG), 1)
	@echo "Execution dans un environnement de developpement"
	@touch ./reports/hadolint/output.json
	@docker build -t todo_dockerfile_check -f ./build/hadolint/Dockerfile .
	@docker run -v ${PWD}:/app todo_dockerfile_check hadolint -f json Dockerfile > ./reports/hadolint/output.json || exit 0
else 
	@hadolint -f json Dockerfile > ./reports/hadolint/output.json
endif


clean_all_reports: 
	@echo "Suppression de tous les rapports"
	@rm -rf ./reports

clean_detect_secrets_reports:
	@echo "Suppression de tous les rapport de detect_secrets"
	@rm -rf ${PWD}/reports/detect_secrets

clean_safety_reports:
	@echo "Suppression de tous les rapports de safety"
	@rm -rf ${PWD}/reports/safety 

clean_flake8_reports:
	@echo "Suppression de tous les rapports flake8"
	@rm -rf ${PWD}/reports/flake8 

clean_unittest_reports:
	@echo "Suppression de tous les rapports unittest..."
	@rm -rf ${PWD}/reports/unittest 

clean_hadolint_reports:
	@echo "Suppression de tous les rapports hadolint"
	@rm -rf ${PWD}/reports/hadolint
	@echo "Fait..."

clean_all_images: clean_detect_secrets_image clean_safety_image clean_flake8_image clean_unittest_image clean_hadolint_image
	@docker rmi -f python:3.8-slim

clean_detect_secrets_image:
	@echo "Suppression de l'image detect_secrets"
	@docker rmi -f todo_scan_for_git_credentials

clean_safety_image:
	@echo "Suppression de l'image safety"
	@docker rmi -f todo_dependancies_check

clean_flake8_image:
	@echo "Suppression de l'image flake8"
	@docker rmi -f todo_code_quality_check

clean_unittest_image:
	@echo "Suppression de l'image unittest"
	@docker rmi -f todo_code_coverage_check

clean_hadolint_image:
	@echo "Suppression de l'image hadolint"
	@docker rmi -f todo_dockerfile_check hadolint/hadolint:2.8.0-alpine

clean_all: clean_all_images clean_all_reports
