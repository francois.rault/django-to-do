from pickle import TRUE
from django.test import TestCase, Client
from tasks.models import Task, Username, TaskForm
from tasks.views import check_user_validity
from http.cookies import SimpleCookie


class TasksTestCase(TestCase):

    def setUp(self):
        self.username = Username.objects.create(
            username="TestUser"
        )
        self.task = Task.objects.create(
            username=self.username,
            title="TestTitle",
            description="TestDescritpion"
        )
        self.client = Client()
        self.client.cookies = SimpleCookie(
            {'username': self.username.username})



    def test_create_tasks(self):
        form_data = {
            "title": "TestTitle",
            "description": "TestDescription",
            "priority": "adanger"
        }
        response = self.client.post(
            "/",
            data=form_data
        )
        self.assertEqual(response.status_code, 200)

    def test_delete_task(self):
        response = self.client.delete(
            f"/delete/{self.task.id}"
        )
        self.assertEqual(response.status_code, 301)

    def test_task_form(self):
        form_data = {
            "title": "TestTitle",
            "description": "TestDescription",
            "priority": "adanger"
        }
        form = TaskForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_task_form_missing_priority(self):
        form_data = {
            "title": "TestTitle",
            "description": "TestDescription",
        }
        form = TaskForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_task_form_missing_title(self):
        form_data = {
            "description": "TestDescription",
            "priority": "adanger"
        }
        form = TaskForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_task_form_missing_description(self):
        form_data = {
            "title": "TestTitle",
            "priority": "adanger"
        }
        form = TaskForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_task_form_wrong_priority(self):
        form_data = {
            "title": "TestTitle",
            "description": "TestDescription",
            "priority": "wrongPriority"
        }
        form = TaskForm(data=form_data)
        self.assertFalse(form.is_valid())

    
